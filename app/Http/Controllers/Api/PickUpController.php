<?php

namespace App\Http\Controllers\Api;

use App\Models\Farmer;
use App\Models\Pickup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PickUpController extends Controller
{
    public function newFarmerPickup(Request $request)
    {

        $this->validate($request,[
            'date'=>'required',
            'no_of_litres'=>'required',
            'account_number'=>'required',
            'farmer_id'=>'required'
        ]);


        $pick=new Pickup();
        $pick->farmer_id=$request->farmer_id;
        $pick->date=$request->date;
        $pick->no_of_litres=$request->no_of_litres;
        $pick->account_number=$request->account_number;
        $pick->agent_id=Auth::id();
        $pick->save();


        return response()->json($pick->load(['farmer','agent']));
    }
    public function allFarmerPickUps(Farmer $farmer)
    {
        return response()->json(Pickup::where('farmer_id',$farmer->id)->with(['farmer','agent'])->get());

    }




}
