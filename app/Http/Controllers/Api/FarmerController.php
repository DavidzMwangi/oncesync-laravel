<?php

namespace App\Http\Controllers\Api;

use App\Models\Farmer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FarmerController extends Controller
{

    public function newFarmer(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'gender' => 'required',
            'telephone_no' => 'required',
            'account_number' => 'required',
            'location' => 'required'
        ]);


        $farmer = new Farmer();
        $farmer->name = $request->name;
        $farmer->gender = $request->gender;
        $farmer->telephone_no = $request->telephone_no;
        $farmer->account_number = $request->account_number;
        $farmer->location = $request->location;
        $farmer->agent_id = Auth::id();
        $farmer->save();


        return response()->json($farmer->load('agent'));
    }

        public function allFarmers()
    {
        return response()->json(Farmer::all()->load('agent'));
    }
}
