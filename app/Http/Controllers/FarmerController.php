<?php

namespace App\Http\Controllers;

use App\Models\Farmer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FarmerController extends Controller
{
    public function saveNewFarmer(Request $request)
    {
        $this->validate($request,[
            'tel_no'=>'required',
            'account_no'=>'required',
            'location'=>'required',
            'gender'=>'required'
        ]);

        $farmer=Farmer::where('user_id',Auth::id())->first();

        if ($farmer==null){

            $farmer=new Farmer();
            $farmer->user_id=Auth::id();
        }

        $farmer->gender=$request->gender;
        $farmer->telephone_no=$request->tel_no;
        $farmer->account_number=$request->account_no;
        $farmer->location=$request->location;
        $farmer->save();

        return redirect()->back();

    }

    public function farmerInfo()
    {
        return view('new_farmer');
    }

    public function farmerProfiles()
    {
        return view('admin.profiles')->withProfiles(Farmer::all()->load('user'));
    }

    public function farmerData()
    {
        $farmer=Farmer::where('user_id',Auth::id())->first()->load('user');

        return response()->json($farmer);
    }


}
