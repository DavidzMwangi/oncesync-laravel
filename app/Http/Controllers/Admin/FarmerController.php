<?php

namespace App\Http\Controllers\Admin;

use App\Models\Farmer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FarmerController extends Controller
{
    public function farmerProfiles()
    {
        return view('admin.profiles')->withProfiles(Farmer::all());
    }


}
