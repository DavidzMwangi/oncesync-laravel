<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pickup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PickUpController extends Controller
{
    public function allPickUps()
    {
        return view('admin.all_pickups')->withPickups(Pickup::all()->load('farmer'));
    }
}
