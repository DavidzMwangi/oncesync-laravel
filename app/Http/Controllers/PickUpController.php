<?php

namespace App\Http\Controllers;

use App\Models\Farmer;
use App\Models\Pickup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PickUpController extends Controller
{
    public function newPickup()
    {
        return view('new_pickup');
    }

    public function saveNewPickup(Request $request)
    {
        $this->validate($request,[
            'pick_up_date'=>'required',
            'no_of_litres'=>'required',
            'acc_no'=>'required',
        ]);

        $farmer=Farmer::where('user_id',Auth::id())->first();


        $pick=new Pickup();
        $pick->date=$request->pick_up_date;
        $pick->no_of_litres=$request->no_of_litres;
        $pick->account_number=$request->acc_no;
        $pick->farmer_id=$farmer->id;

        $pick->save();


        return redirect()->route('pickup.all_pickups');
    }

    public function allFarmerPickUps()
    {
        $farmer=Farmer::where('user_id',Auth::id())->first();


        return response()->json(Pickup::where('farmer_id',$farmer->id)->get());

    }

    public function allPickUps()
    {
        return view('admin.all_pickups')->withPickups(Pickup::all()->load('farmer'));
    }

    public function newFarmerPickup(Request $request)
    {

        $this->validate($request,[
            'date'=>'required',
            'no_of_litres'=>'required',
            'account_number'=>'required'
        ]);
        $farmer=Farmer::where('user_id',Auth::id())->first();


        $pick=new Pickup();
        $pick->farmer_id=$farmer->id;
        $pick->date=$request->date;
        $pick->no_of_litres=$request->no_of_litres;
        $pick->account_number=$request->account_number;
        $pick->save();


        return response()->json($pick);
    }


}
