<?php

namespace App\Http\Middleware;

use App\Models\Farmer;
use Closure;
use Illuminate\Support\Facades\Auth;

class UserChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user=Auth::user();

        if ($user->user_type==1){
            //agent
            return redirect()->route('log_out_agent');

        }else{
            //admin
            return $next($request);

        }
//        $farmer=Farmer::where('user_id',Auth::id())->first();
//        if ($farmer!=null){
//
//            return $next($request);
//
//        }else{
//
//            return redirect()->route('new_farmer_info');
//        }
    }
}
