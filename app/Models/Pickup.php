<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Pickup extends Model
{
    public function Farmer()
    {
        return $this->hasOne(Farmer::class,'id','farmer_id');
    }


    public function Agent()
    {
        return $this->hasOne(User::class,'id','agent_id');
    }
}
