<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
ROute::get('log_out_agent',function (){

    return json_encode('please log in as an admin');
})->name('log_out_agent');
Route::get('/home', 'HomeController@index')->name('home')->middleware(['auth','user_checker']);

Route::group(['namespace'=>'Admin','prefix'=>'admin','as'=>'admin.','middleware'=>['auth','user_checker']],function (){
    Route::group(['prefix'=>'pickup','as'=>'pickup.'],function (){
        Route::get('all_pickups','PickUpController@allPickUps')->name('all_pickups');
    });
    Route::get('profiles','FarmerController@farmerProfiles')->name('profiles');

});







Route::get('new_farmer_info','FarmerController@farmerInfo')->name('new_farmer_info')->middleware('auth'); //ensures the user has already
Route::post('save_farmer_info','FarmerController@saveNewFarmer')->name('save_farmer_info');

Route::group(['middleware'=>['auth','user_checker']],function (){


    Route::get('farmer_info','FarmerController@farmerInfo')->name('farmer_info');



    Route::get('profiles','FarmerController@farmerProfiles')->name('profiles');
});


