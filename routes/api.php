<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace'=>'Api'],function () {

    Route::group(['namespace' => 'Auth'], function () {
        Route::post('login', 'LoginController@login');
        Route::post('register', 'RegisterController@register');
        Route::post('recover_password', 'LoginController@recoverPassword');

    });
    Route::group(['middleware'=>'auth:api'],function (){

        //agent module
        Route::post('new_farmer','FarmerController@newFarmer');
        Route::post('new_farmer_pickup','PickUpController@newFarmerPickup');
        Route::get('farmer_pick_ups/{farmer}','PickUpController@allFarmerPickUps');
        Route::get('all_farmers','FarmerController@allFarmers');
    });
});



