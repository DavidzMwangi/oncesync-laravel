@extends('layouts.master')
@section('style')

    <!-- Custom styles for this template -->
    <link href="{{asset('template/css/sb-admin-2.min.css')}}" rel="stylesheet">


    <!-- Custom styles for this page -->
    <link href="{{asset('template/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

@endsection
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Farmer</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Farmer Details</h6>
            </div>
            <div class="card-body">

                <form method="post" action="{{route('save_farmer_info')}}">
                    {{csrf_field()}}

                    <div class="row">
                        <div class="form-group col-6">

                            <label>Telephone Number</label>
                            <input type="text" class="form-control" name="tel_no">
                        </div>


                        <div class="form-group col-6">

                            <label>Account Number</label>
                            <input type="text" class="form-control" name="account_no">
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-6">

                            <label>Location</label>
                            <input type="text" class="form-control" name="location">
                        </div>


                        <div class="form-group col-6">

                            <label>Gender</label>
                            <select class="form-control" name="gender">
                                <option value="0">Male</option>
                                <option value="1">Female</option>
                            </select>
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group col-6">

                            <button class="btn btn-primary" type="submit" >Submit</button>

                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')


    <!-- Custom scripts for all pages-->
    <script src="{{asset('template/js/sb-admin-2.min.js')}}"></script>

    <!-- Page level plugins -->
    <script src="{{asset('template/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('template/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('template/js/demo/datatables-demo.js')}}"></script>

@endsection