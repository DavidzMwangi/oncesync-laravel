@extends('layouts.master')
@section('style')

    <!-- Custom styles for this template -->
    <link href="{{asset('template/css/sb-admin-2.min.css')}}" rel="stylesheet">


    <!-- Custom styles for this page -->
    <link href="{{asset('template/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

@endsection
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Farmers</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">All Farmers</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Farmer Name</th>
                            <th>Agent Name(Agent who added the farmer)</th>
                            <th>Farmer Account Number</th>
                            <th>Location</th>
                            <th>Gender</th>
                            <th>Telephone Number</th>

                        </tr>
                        </thead>

                        <tbody>

                        @foreach($profiles as $farmer)

                        <tr>
                            <td>{{$farmer->name}}</td>
                            <td>{{$farmer->agent->name}}</td>
                            <td>{{$farmer->account_number}}</td>
                            <td>{{$farmer->location}}</td>
                            <td>
                                @if($farmer->gender==0)
                                    <span class="badge badge-primary">Male</span>

                                    @else
                                    <span class="badge badge-primary">Female</span>

                                @endif
                                </td>
                            <td>{{$farmer->telephone_no}}</td>


                        </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')


    <!-- Custom scripts for all pages-->
    <script src="{{asset('template/js/sb-admin-2.min.js')}}"></script>

    <!-- Page level plugins -->
    <script src="{{asset('template/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('template/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('template/js/demo/datatables-demo.js')}}"></script>

@endsection