@extends('layouts.master')
@section('style')

    <!-- Custom styles for this template -->
    <link href="{{asset('template/css/sb-admin-2.min.css')}}" rel="stylesheet">


    <!-- Custom styles for this page -->
    <link href="{{asset('template/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">

@endsection
@section('content')
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Pickup</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">New Pickup</h6>
            </div>
            <div class="card-body">

                <form method="post" action="{{route('pickup.save_new_pickup')}}">
                    {{csrf_field()}}

                    <div class="row">
                        <div class="form-group col-6">

                            <label>PickUp Date</label>
                            <input type="date" class="form-control" name="pick_up_date" required>
                        </div>


                        <div class="form-group col-6">

                            <label>Number of Litres</label>
                            <input type="number" class="form-control" name="no_of_litres" required>
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group col-6">

                            <label>Account Number</label>
                            <input type="text" class="form-control" name="acc_no" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">

                            <button class="btn btn-primary" type="submit" >Submit</button>

                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
@section('script')


    <!-- Custom scripts for all pages-->
    <script src="{{asset('template/js/sb-admin-2.min.js')}}"></script>

    <!-- Page level plugins -->
    <script src="{{asset('template/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('template/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Page level custom scripts -->
    <script src="{{asset('template/js/demo/datatables-demo.js')}}"></script>

@endsection